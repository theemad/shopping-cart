export interface UpdateOrderInterface {
  productId: string;

  newQuantity: number;
}
