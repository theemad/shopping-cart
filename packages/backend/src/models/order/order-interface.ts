import { ProductInterface } from '../product';

export interface OrderInterface {
  id: string;

  products: ProductInterface[];

  totalPrice: number;
}
