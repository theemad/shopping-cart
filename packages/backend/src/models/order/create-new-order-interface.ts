export interface CreateNewOrderInterface {
  products: { productId: string; quantity: number }[];
}
