import { App } from 'aws-cdk-lib';
import { BackendStack } from './backend-stack';

const app = new App();
new BackendStack(app, 'shopping-cart-backend', {
  env: { region: 'eu-central-1', account: '788848846223' },
});
