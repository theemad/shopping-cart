import { Handler } from 'aws-cdk-lib/aws-lambda';
import { APIGatewayProxyResult } from 'aws-lambda';
import { failure, success } from '../../libs';
import { ProductService } from '../../services';

export const handler: Handler = async (): Promise<APIGatewayProxyResult> => {
  const productService = new ProductService();

  try {
    const products = await productService.listProducts();
    return success(products);
  } catch (err) {
    return failure(err as never);
  }
};
