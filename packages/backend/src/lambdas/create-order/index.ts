import { Handler } from 'aws-cdk-lib/aws-lambda';

import { v4 } from 'uuid';
import * as AWS from 'aws-sdk';

import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { failure, success, validationFailure } from '../../libs';
import {
  CreateNewOrderInterface,
  OrderInterface,
  ProductInterface,
} from '../../models';
import { StatusCodes } from 'http-status-codes';

const db = new AWS.DynamoDB.DocumentClient();
const ORDERS_TABLE = process.env.ORDERS_TABLE || '';
const PRODUCTS_TABLE = process.env.PRODUCTS_TABLE || '';

export const handler: Handler = async (
  event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
  const { products } = JSON.parse(event.body ?? '') as CreateNewOrderInterface;

  const newOrder: OrderInterface = {
    id: v4(),
    products: [],
    totalPrice: 0,
  };

  const selectedProducts: (ProductInterface & { selectedQuantity: number })[] =
    [];

  try {
    if (!products?.length) {
      return validationFailure(
        StatusCodes.BAD_REQUEST,
        'Products array cannot be empty.'
      );
    }

    for (const product of products) {
      const { productId, quantity } = product;

      // Checking whether all the ordered items are present in the inventory and are available in the desired quantity.
      const { Item } = await db
        .get({
          TableName: PRODUCTS_TABLE,
          Key: { id: productId },
        })
        .promise();

      if (!Item) {
        return validationFailure(
          StatusCodes.BAD_REQUEST,
          `Product with id ${productId} was not found.`
        );
      }

      if (Item.quantity < quantity) {
        return validationFailure(
          StatusCodes.BAD_REQUEST,
          `Product with id ${productId} is out of stock.`
        );
      }

      selectedProducts.push({
        ...(Item as ProductInterface),
        selectedQuantity: quantity,
      });

      newOrder.products.push({
        ...(Item as ProductInterface),
        quantity: quantity,
      });
      newOrder.totalPrice += Item.price * quantity;
    }

    // Update product quantities
    for (const product of selectedProducts) {
      const params = {
        TableName: PRODUCTS_TABLE,
        Key: { id: product.id },
        UpdateExpression: 'set quantity = :newquantity',
        ExpressionAttributeValues: {
          ':newquantity': product.quantity - product.selectedQuantity,
        },
      };

      db.update(params).promise();
    }

    await db
      .put({
        TableName: ORDERS_TABLE,
        Item: newOrder,
      })
      .promise();
    return success(
      `Order with ID: ${newOrder.id} has been created successfully`
    );
  } catch (err) {
    return failure(err as never);
  }
};
