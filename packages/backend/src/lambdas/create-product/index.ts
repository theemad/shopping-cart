import { Handler } from 'aws-cdk-lib/aws-lambda';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { v4 } from 'uuid';
import * as AWS from 'aws-sdk';
import { failure, success, validationFailure } from '../../libs';
import { ProductInterface } from '../../models';
import { StatusCodes } from 'http-status-codes';

const db = new AWS.DynamoDB.DocumentClient();
const PRODUCTS_TABLE = process.env.PRODUCTS_TABLE || '';

export const handler: Handler = async (
  event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
  const dto = JSON.parse(event.body ?? '') as Omit<ProductInterface, 'id'>;

  //validation
  if (!dto?.name) {
    return validationFailure(
      StatusCodes.BAD_REQUEST,
      'The product name is required.'
    );
  }

  if (!dto?.quantity) {
    return validationFailure(
      StatusCodes.BAD_REQUEST,
      'The product quantity is required.'
    );
  }

  if (!dto?.price) {
    return validationFailure(
      StatusCodes.BAD_REQUEST,
      'The Product price is required.'
    );
  }

  const newProduct: ProductInterface = {
    id: v4(),
    ...dto,
  };

  const params = {
    TableName: PRODUCTS_TABLE,
    Item: newProduct,
  };

  try {
    await db.put(params).promise();
    return success(
      `The product with ID: ${newProduct.id} has been created successfully.`,
      StatusCodes.CREATED
    );
  } catch (err) {
    return failure(err as never);
  }
};
