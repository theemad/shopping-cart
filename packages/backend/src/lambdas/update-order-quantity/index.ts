import { Handler } from 'aws-cdk-lib/aws-lambda';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { failure, success, validationFailure } from '../../libs';

import { StatusCodes } from 'http-status-codes';
import { UpdateOrderInterface } from '../../models';
import { OrderService, ProductService } from '../../services';

export const handler: Handler = async (
  event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
  const orderId = event.pathParameters?.orderId ?? '';
  const orderService = new OrderService(orderId);
  const productService = new ProductService();
  const { productId, newQuantity } = JSON.parse(
    event.body ?? ''
  ) as UpdateOrderInterface;

  try {
    const order = await orderService.getOrder();

    if (!order) {
      return validationFailure(
        StatusCodes.BAD_REQUEST,
        `order with Id: ${orderId} was not found`
      );
    }

    const product = await productService.getProduct(productId);
    const orderedProduct = order.products.find(
      (product) => product.id === productId
    );

    if (!product || !orderedProduct) {
      return validationFailure(
        StatusCodes.BAD_REQUEST,
        `product with Id: ${productId} was not found`
      );
    }

    if (newQuantity > product.quantity) {
      return validationFailure(
        StatusCodes.BAD_REQUEST,
        `product with Id: ${orderId} is out of stock`
      );
    }

    const products = order.products;

    const newProducts = products.map((product) => {
      if (product.id === productId) {
        return { ...product, quantity: newQuantity };
      }
      return product;
    });

    //Update total price
    const newTotalPrice = newProducts.reduce((totalPrice, product) => {
      return totalPrice + product.price * product.quantity;
    }, 0);

    await orderService.updateProducts(newProducts, newTotalPrice);

    return success(`order with id ${orderId} has been updated successfully`);
  } catch (err) {
    return failure(err as never);
  }
};
