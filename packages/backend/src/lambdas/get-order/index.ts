import { Handler } from 'aws-cdk-lib/aws-lambda';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { failure, success, validationFailure } from '../../libs';
import { OrderService } from '../../services';
import { StatusCodes } from 'http-status-codes';

export const handler: Handler = async (
  event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
  const orderId = event.pathParameters?.orderId ?? '';
  const orderService = new OrderService(orderId);
  try {
    const order = await orderService.getOrder();
    if (!order) {
      return validationFailure(
        StatusCodes.BAD_REQUEST,
        `order with Id: ${orderId} was not found`
      );
    }
    return success(order);
  } catch (err) {
    return failure(err as never);
  }
};
