import { IResource } from 'aws-cdk-lib/aws-apigateway';
import { Table } from 'aws-cdk-lib/aws-dynamodb';

export interface CommonProps {
  resource: IResource;

  ordersTable: Table;

  productsTable: Table;
}
