import { Stack, StackProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';
import {
  BackendRestApi,
  Database,
  OrdersService,
  ProductsService,
} from './constructs';
import { CommonProps } from './types';

export class BackendStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const backendRestApi = new BackendRestApi(this, 'rest-api');
    const database = new Database(this, 'database');

    const commonProps: CommonProps = {
      resource: backendRestApi.restApi.root,
      ordersTable: database.ordersTable,
      productsTable: database.productsTable,
    };

    new OrdersService(this, 'orders-service', commonProps);
    new ProductsService(this, 'products-service', commonProps);
  }
}
