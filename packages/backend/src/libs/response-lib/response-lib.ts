import { StatusCodes } from 'http-status-codes';
import { APIGatewayProxyResult } from 'aws-lambda';

const buildResponse = (
  statusCode: number,
  body: unknown
): APIGatewayProxyResult => ({
  statusCode: statusCode,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': true,
  },
  body: JSON.stringify(body),
});

export const success = (
  body: unknown,
  statusCode: number = StatusCodes.OK
): APIGatewayProxyResult => {
  return buildResponse(statusCode, body);
};

export const failure = (
  error: Error & {
    httpStatus: number;
    apiMessage?: string;
  }
): APIGatewayProxyResult => {
  console.error(error);
  const { httpStatus } = error;
  const dto = {
    status: 'ERROR',
    message: error?.apiMessage ?? undefined,
  };
  return buildResponse(httpStatus || StatusCodes.INTERNAL_SERVER_ERROR, dto);
};

export const validationFailure = (
  httpStatus: number,
  apiMessage?: string
): APIGatewayProxyResult => {
  const dto = {
    message: apiMessage ?? undefined,
  };
  return buildResponse(httpStatus, dto);
};
