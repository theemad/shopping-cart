import { Construct } from 'constructs';
import { IFunction, Runtime } from 'aws-cdk-lib/aws-lambda';
import { CommonProps } from '../types';
import { NodejsFunction } from 'aws-cdk-lib/aws-lambda-nodejs';
import { Duration } from 'aws-cdk-lib';
import * as path from 'path';
import { LambdaIntegration } from 'aws-cdk-lib/aws-apigateway';

export class ProductsService extends Construct {
  private readonly runtime = Runtime.NODEJS_14_X;

  constructor(scope: Construct, id: string, props: CommonProps) {
    super(scope, id);
    this.createApiRoutes(props);
  }

  private createApiRoutes(props: CommonProps): void {
    const productsRoot = props.resource.addResource('products');

    const createProduct = this.createProductLambda(props);
    productsRoot.addMethod('POST', new LambdaIntegration(createProduct));

    const listProducts = this.listProductsLambda(props);
    productsRoot.addMethod('GET', new LambdaIntegration(listProducts));
  }

  private createProductLambda({ productsTable }: CommonProps): IFunction {
    const lambda = new NodejsFunction(this, 'create-product', {
      functionName: 'create-product',
      runtime: this.runtime,
      handler: 'handler',
      entry: path.join(__dirname, '/../lambdas/create-product/index.ts'),
      memorySize: 512,
      timeout: Duration.seconds(3),
      environment: {
        PRODUCTS_TABLE: productsTable.tableName,
      },
    });
    productsTable.grantWriteData(lambda);
    productsTable.grant(lambda, 'dynamodb:DescribeTable');

    return lambda;
  }

  private listProductsLambda({ productsTable }: CommonProps): IFunction {
    const lambda = new NodejsFunction(this, 'list-products', {
      functionName: 'list-products',
      runtime: this.runtime,
      handler: 'handler',
      entry: path.join(__dirname, '/../lambdas/list-products/index.ts'),
      memorySize: 512,
      timeout: Duration.seconds(3),
      environment: {
        PRODUCTS_TABLE: productsTable.tableName,
      },
    });
    productsTable.grantReadData(lambda);
    productsTable.grant(lambda, 'dynamodb:DescribeTable');

    return lambda;
  }
}
