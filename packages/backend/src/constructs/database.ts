import { Construct } from 'constructs';
import {
  Attribute,
  AttributeType,
  BillingMode,
  Table,
  TableEncryption,
} from 'aws-cdk-lib/aws-dynamodb';
import { RemovalPolicy } from 'aws-cdk-lib';

export class Database extends Construct {
  public readonly ordersTable: Table;

  public readonly productsTable: Table;
  constructor(scope: Construct, id: string) {
    super(scope, id);

    this.ordersTable = this.createTable('orders', {
      type: AttributeType.STRING,
      name: 'id',
    });

    this.productsTable = this.createTable('products', {
      type: AttributeType.STRING,
      name: 'id',
    });
  }

  private createTable(
    tableName: string,
    partitionKey: Attribute,
    sortKey?: Attribute
  ): Table {
    return new Table(this, `table-${tableName}`, {
      tableName: tableName,
      removalPolicy: RemovalPolicy.RETAIN,
      billingMode: BillingMode.PAY_PER_REQUEST,
      encryption: TableEncryption.AWS_MANAGED,
      partitionKey,
      sortKey,
    });
  }
}
