import { Cors, RestApi } from 'aws-cdk-lib/aws-apigateway';
import { Construct } from 'constructs';

export class BackendRestApi extends Construct {
  public restApi: RestApi;

  constructor(scope: Construct, id: string) {
    super(scope, id);

    this.restApi = this.createRestApi();
  }

  private createRestApi(): RestApi {
    return new RestApi(this, 'rest-api', {
      restApiName: 'shopping-cart-rest-api',
      deployOptions: {
        stageName: 'api',
        throttlingBurstLimit: 100,
        throttlingRateLimit: 100,
      },
      defaultCorsPreflightOptions: {
        allowOrigins: ['*'],
        allowMethods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'],
        allowHeaders: Cors.DEFAULT_HEADERS,
      },
    });
  }
}
