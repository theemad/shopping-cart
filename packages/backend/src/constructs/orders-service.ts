import { Construct } from 'constructs';
import { IFunction, Runtime } from 'aws-cdk-lib/aws-lambda';
import { CommonProps } from '../types';
import { NodejsFunction } from 'aws-cdk-lib/aws-lambda-nodejs';
import { Duration } from 'aws-cdk-lib';
import * as path from 'path';
import { LambdaIntegration } from 'aws-cdk-lib/aws-apigateway';
import { createOrderValidation } from '../utils';

export class OrdersService extends Construct {
  private readonly runtime = Runtime.NODEJS_14_X;

  constructor(scope: Construct, id: string, props: CommonProps) {
    super(scope, id);
    this.createApiRoutes(props);
  }

  private createApiRoutes(props: CommonProps): void {
    const ordersRoot = props.resource.addResource('orders');

    const getOrder = this.getOrderLambda(props);
    const orderRoot = ordersRoot.addResource('{orderId}');

    orderRoot.addMethod('GET', new LambdaIntegration(getOrder), {
      requestParameters: {
        'method.request.path.orderId': true,
      },
    });

    const updateOrder = this.updateOrderQuantityLambda(props);
    orderRoot.addMethod('PATCH', new LambdaIntegration(updateOrder), {
      requestParameters: {
        'method.request.path.orderId': true,
      },
    });

    const createOrderModel = createOrderValidation(this, props.resource.api);
    const createOrder = this.createOrderLambda(props);
    ordersRoot.addMethod('POST', new LambdaIntegration(createOrder), {
      requestValidatorOptions: {
        requestValidatorName: 'create-order-validator',
        validateRequestBody: true,
      },
      requestModels: {
        'application/json': createOrderModel,
      },
    });
  }

  private createOrderLambda({
    ordersTable,
    productsTable,
  }: CommonProps): IFunction {
    const lambda = new NodejsFunction(this, 'create-order', {
      functionName: 'create-order',
      runtime: this.runtime,
      handler: 'handler',
      entry: path.join(__dirname, '/../lambdas/create-order/index.ts'),
      memorySize: 512,
      timeout: Duration.seconds(3),
      environment: {
        ORDERS_TABLE: ordersTable.tableName,
        PRODUCTS_TABLE: productsTable.tableName,
      },
    });

    ordersTable.grantWriteData(lambda);
    ordersTable.grant(lambda, 'dynamodb:DescribeTable');
    productsTable.grantReadWriteData(lambda);
    productsTable.grant(lambda, 'dynamodb:DescribeTable');

    return lambda;
  }

  private getOrderLambda({ ordersTable }: CommonProps): IFunction {
    const lambda = new NodejsFunction(this, 'get-order', {
      functionName: 'get-order',
      runtime: this.runtime,
      handler: 'handler',
      entry: path.join(__dirname, '/../lambdas/get-order/index.ts'),
      memorySize: 512,
      timeout: Duration.seconds(3),
      environment: {
        ORDERS_TABLE: ordersTable.tableName,
      },
    });
    ordersTable.grantReadData(lambda);
    ordersTable.grant(lambda, 'dynamodb:DescribeTable');

    return lambda;
  }

  private updateOrderQuantityLambda({
    ordersTable,
    productsTable,
  }: CommonProps): IFunction {
    const lambda = new NodejsFunction(this, 'update-order-quantity', {
      functionName: 'update-order-quantity',
      runtime: this.runtime,
      handler: 'handler',
      entry: path.join(__dirname, '/../lambdas/update-order-quantity/index.ts'),
      memorySize: 512,
      timeout: Duration.seconds(3),
      environment: {
        ORDERS_TABLE: ordersTable.tableName,
        PRODUCTS_TABLE: productsTable.tableName,
      },
    });
    ordersTable.grantReadWriteData(lambda);
    ordersTable.grant(lambda, 'dynamodb:DescribeTable');
    productsTable.grantReadData(lambda);
    productsTable.grant(lambda, 'dynamodb:DescribeTable');

    return lambda;
  }
}
