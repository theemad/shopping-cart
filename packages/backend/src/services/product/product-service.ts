import * as AWS from 'aws-sdk';
import { ProductInterface } from '../../models';

const PRODUCTS_TABLE = process.env.PRODUCTS_TABLE || '';
const db = new AWS.DynamoDB.DocumentClient();
export class ProductService {
  public async listProducts(): Promise<ProductInterface[]> {
    const { Items } = await db.scan({ TableName: PRODUCTS_TABLE }).promise();
    return Items as ProductInterface[];
  }

  public async getProduct(
    productId: string
  ): Promise<ProductInterface | undefined> {
    const { Item } = await db
      .get({
        TableName: PRODUCTS_TABLE,
        Key: { id: productId },
      })
      .promise();

    return Item as ProductInterface;
  }
}
