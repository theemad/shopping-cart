import { OrderInterface, ProductInterface } from '../../models';
import * as AWS from 'aws-sdk';

const db = new AWS.DynamoDB.DocumentClient();

const ORDERS_TABLE = process.env.ORDERS_TABLE || '';
export class OrderService {
  private readonly orderId: string;

  constructor(orderId: string) {
    this.orderId = orderId;
  }

  public async getOrder(): Promise<OrderInterface | undefined> {
    const { Item } = await db
      .get({
        TableName: ORDERS_TABLE,
        Key: { id: this.orderId },
      })
      .promise();

    return Item as OrderInterface;
  }

  public async updateProducts(
    products: ProductInterface[],
    totalPrice: number
  ): Promise<void> {
    const params = {
      TableName: ORDERS_TABLE,
      Key: { id: this.orderId },
      UpdateExpression:
        'set products = :newProducts, totalPrice = :newTotalPrice',

      ExpressionAttributeValues: {
        ':newProducts': products,
        ':newTotalPrice': totalPrice,
      },
    };

    db.update(params).promise();
  }
}
