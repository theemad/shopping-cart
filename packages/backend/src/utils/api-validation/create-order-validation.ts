import { JsonSchemaType, Model } from 'aws-cdk-lib/aws-apigateway';
import { Construct } from 'constructs';
import { IRestApi } from 'aws-cdk-lib/aws-apigateway/lib/restapi';

export const createOrderValidation = (scope: Construct, api: IRestApi) => {
  return new Model(scope, 'model-validator', {
    restApi: api,
    contentType: 'application/json',
    description: 'To validate the request body',
    modelName: 'createordermodel',
    schema: {
      type: JsonSchemaType.OBJECT,
      required: ['products'],
      properties: {
        products: {
          type: JsonSchemaType.ARRAY,
          required: ['productId', 'quantity'],
          properties: {
            productId: { type: JsonSchemaType.STRING },
            quantity: {
              type: JsonSchemaType.NUMBER,
            },
          },
        },
      },
    },
  });
};
